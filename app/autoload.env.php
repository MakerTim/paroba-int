<?php

/**
 * @file
 * Loads the .env file.
 *
 * This file is included very early. See autoload.files in composer.json and
 * https://getcomposer.org/doc/04-schema.md#files
 */

use Dotenv\Dotenv;

$dotenv = Dotenv::createUnsafeMutable(dirname(__DIR__));

if (getenv('APP_ENV') === FALSE || getenv('APP_ENV') === 'dev') {
  $dotenv->load();
}

if (PHP_SAPI === 'cli') {
  ini_set('memory_limit', '512M');
}

if (PHP_SAPI !== 'cli') {
  $dotenv
    ->required([
      'APP_DOMAIN',
      'APP_URL',
      'APP_SECRET',
      'DB_HOST',
      'DB_NAME',
      'DB_USER',
      'DB_PASS'
    ])
    ->notEmpty();
}
