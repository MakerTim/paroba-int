<?php

namespace Drupal\custom_base\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigExtension extends AbstractExtension {

  public function getFilters(): array {
    return [
      new TwigFilter('valid_anchor', [$this, 'validAnchor']),
    ];
  }

  public function getName(): string {
    return 'custom_base.twig_extension';
  }

  public function validAnchor(string $anchor): string {
    return custom_base_valid_anchor($anchor);
  }

}
