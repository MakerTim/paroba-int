<?php

namespace Drupal\custom_base\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\custom_base\EntitySanitizer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class HtmlConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  public const TAGS_DISALLOWED = [
    'div',
    'span',
  ];

  public function __construct(
    private readonly EntitySanitizer $sanitizer
  ) {}

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('custom_base.entity_sanitizer')
    );
  }

  public function validate(mixed $value, Constraint $constraint): void {
    assert($value instanceof ContentEntityInterface);
    assert($constraint instanceof HtmlConstraint);

    $entity = $value;
    // Do not act on syncing operations (migration/import/...)
    if ($entity->isSyncing()) {
      return;
    }

    // Sanitize before validation; only unfixable violations will be returned.
    $this->sanitizer->sanitizeHtml($entity);

    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if ($field_definition->isComputed()) {
        continue;
      }

      match ($field_definition->getType()) {
        'text_long',
        'text_with_summary',
        'text' => $this->validateTextField($entity->get($field_name), $constraint),
        default => NULL,
      };
    }
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface<\Drupal\Core\Field\FieldItemInterface> $field
   */
  private function validateTextField(FieldItemListInterface $field, HtmlConstraint $constraint): void {
    foreach ($field as $delta => $item) {
      if ($item->isEmpty()) {
        continue;
      }

      assert($item instanceof FieldItemInterface && isset($item->value) && is_string($item->value));

      $this->validateText("{$field->getName()}.$delta.value", $item->value, $constraint);
    }
  }

  private function validateText(string $property, string $text, HtmlConstraint $constraint): void {
    foreach (self::TAGS_DISALLOWED as $disallowed_tag) {
      if (str_contains($text, '<' . $disallowed_tag)) {
        $this->addViolation($property, $disallowed_tag, $constraint->messageInvalidTag);
      }
    }
  }

  private function addViolation(string $property, string $tag, string $message, array $parameters = []): void {
    $parameters += [
      '%tag' => $tag,
    ];

    $this->context->buildViolation($message, $parameters)
      ->atPath($property)
      ->addViolation();
  }

}
