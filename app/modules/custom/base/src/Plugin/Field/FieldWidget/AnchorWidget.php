<?php

namespace Drupal\custom_base\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "custom_base_anchor",
 *   label = @Translation("Anchor (Custom)"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class AnchorWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Field\FieldItemListInterface<\Drupal\Core\Field\Plugin\Field\FieldType\StringItem> $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['value']['#pattern'] = '[a-z0-9]([a-z0-9\-]*[a-z0-9])?';

    return $element;
  }

}
