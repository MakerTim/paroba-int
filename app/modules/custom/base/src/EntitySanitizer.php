<?php

namespace Drupal\custom_base;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\custom_base\Plugin\Validation\Constraint\HtmlConstraintValidator;

final class EntitySanitizer {

  public function sanitizeHtml(ContentEntityInterface $entity): void {
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if ($field_definition->isComputed()) {
        continue;
      }

      if ($field_definition->getType() === 'entity_reference_revisions'
        && $field_definition->getItemDefinition()
          ->getSetting('target_type') === 'paragraph'
      ) {
        foreach ($entity->get($field_name)->referencedEntities() as $paragraph) {
          $this->sanitizeHtml($paragraph);
        }
        continue;
      }

      match ($field_definition->getType()) {
        'text_long',
        'text_with_summary',
        'text' => $this->sanitizeTextFieldHtml($field_name, $entity),
        default => NULL,
      };
    }
  }

  private function sanitizeTextFieldHtml(string $field_name, ContentEntityInterface $entity): void {
    if ($entity->get($field_name)->isEmpty()) {
      return;
    }

    $value = $entity->get($field_name)->value;
    assert(is_string($value));

    $entity->get($field_name)->value = preg_replace(
      sprintf("/<\/?\s*(%s)(\s+[^>]*)?>/mi", implode('|', HtmlConstraintValidator::TAGS_DISALLOWED)),
      "",
      $value
    );
  }

}
