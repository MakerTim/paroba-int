<?php

namespace Drupal\Tests\custom_title_hidden\Unit\EventSubscriber;

use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * @coversDefaultClass \Drupal\custom_title_hidden\Plugin\Condition\TitleHiddenCondition
 * @group custom_title_hidden
 */
class TitleHiddenConditionTest extends EntityKernelTestBase {

  protected static $modules = ['node', 'custom_title_hidden'];

  protected function setUp(): void {
    parent::setUp();

    NodeType::create([
      'type' => 'with_title_hidden',
      'name' => 'with_title_hidden',
    ])->save();
    NodeType::create([
      'type' => 'without_title_hidden',
      'name' => 'without_title_hidden',
    ])->save();
    FieldStorageConfig::create([
      'field_name' => 'title_hidden',
      'entity_type' => 'node',
      'type' => 'boolean',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'title_hidden',
      'bundle' => 'with_title_hidden',
    ])->save();
  }

  /**
   * Tests that condition evaluates correctly.
   *
   * @dataProvider providerTestEvaluate
   *
   * @covers ::evaluate
   */
  public function testEvaluate(bool $expected, ?array $node): void {
    /** @var \Drupal\Core\Condition\ConditionPluginBase $condition */
    $condition = \Drupal::service('plugin.manager.condition')
      ->createInstance('custom_title_hidden')
      ->setConfiguration([
        'enabled' => TRUE,
      ])
      ->setContextMapping([
        'node' => 'node',
      ]);

    if (is_array($node)) {
      $contexts['node'] = EntityContext::fromEntity(Node::create($node));
    }
    else {
      $contexts['node'] = EntityContext::fromEntityTypeId('node');
    }

    \Drupal::service('context.handler')->applyContextMapping($condition, $contexts);
    $this->assertEquals($expected, $condition->execute());
  }

  /**
   * Data provider for testEvaluate().
   */
  public function providerTestEvaluate(): array {
    return [
      [FALSE, NULL],
      [FALSE, ['type' => 'without_title_hidden']],
      [FALSE, ['type' => 'with_title_hidden']],
      [FALSE, ['type' => 'with_title_hidden', 'title_hidden' => FALSE]],
      [TRUE, ['type' => 'with_title_hidden', 'title_hidden' => TRUE]],
    ];
  }

}
