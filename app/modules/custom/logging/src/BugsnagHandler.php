<?php

namespace Drupal\custom_logging;

use Bugsnag\Breadcrumbs\Breadcrumb;
use Bugsnag\Client;
use Bugsnag\Report;
use Bugsnag\Stacktrace;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Throwable;

final class BugsnagHandler extends AbstractProcessingHandler {

  const SEVERITY_MAPPING = [
    Logger::DEBUG => 'info',
    Logger::INFO => 'info',
    Logger::NOTICE => 'info',
    Logger::WARNING => 'warning',
    Logger::ERROR => 'error',
    Logger::CRITICAL => 'error',
    Logger::ALERT => 'error',
    Logger::EMERGENCY => 'error',
  ];

  private Client $client;

  private int $notifyLevel;

  private int $notifyPhpErrorLevel;

  public function __construct(Client $client, $level = Logger::ERROR, bool $bubble = TRUE) {
    parent::__construct($level, $bubble);
    $this->client = $client;
    $this->notifyLevel = Logger::ERROR;
    $this->notifyPhpErrorLevel = Logger::NOTICE;
  }

  protected function write(array $record): void {
    $severity = $this->getSeverity($record['level']);
    $context = $record['context'];

    // The php channel is used for PHP errors and uncatched exceptions. In
    // Bugsnag we display these errors/exceptions as unhandled.
    if ($record['channel'] === 'php' && $record['level'] >= $this->notifyPhpErrorLevel) {
      if (isset($context['exception']) && $context['exception'] instanceof Throwable) {
        $this->writeException($record, $severity, TRUE);
      }
      else {
        $this->writePhpError($record, $severity);
      }
    }
    elseif ($record['level'] < $this->notifyLevel) {
      $this->client->leaveBreadcrumb(sprintf('Log(%s).%s', $record['channel'], $record['level_name']), Breadcrumb::LOG_TYPE, array_merge([
        'message' => preg_replace('/(\S)\S*(@\S+)/', '$1[FILTERED]$2', (string) $record['message']),
      ], $record['context']));
    }
    elseif (isset($context['exception']) && $context['exception'] instanceof Throwable) {
      $this->writeException($record, $severity);
    }
    else {
      $this->writeError($record, $severity);
    }
  }

  private function writeException(array $record, string $severity, bool $unhandled = FALSE): void {
    $report = Report::fromPHPThrowable($this->client->getConfig(), $record['context']['exception']);
    $report->setSeverity($severity);
    $report->setUnhandled($unhandled);
    if (isset($record['extra'])) {
      $report->setMetaData($record['extra']);
    }

    $this->client->notify($report);
  }

  private function writePhpError(array $record, string $severity): void {
    $backtrace = $record['context']['backtrace'] ?? NULL;
    $type = $record['drupal_context_placeholders']['%type'] ?? NULL;
    $message = (string) ($record['drupal_context_placeholders']['@message'] ?? $record['message']);
    $name = isset($type, $backtrace)
      // See drupal_error_levels().
      ? sprintf('PHP %s', $type)
      // Fallback, used when logging directly to the php channel.
      : sprintf('Log(%s).%s', $record['channel'], $record['level_name']);

    $report = Report::fromNamedError($this->client->getConfig(), $name, $message);
    $report->setSeverity($severity);
    $report->setSeverityReason(['type' => 'handledError']);
    $report->setUnhandled(TRUE);
    if (isset($record['extra'])) {
      $report->setMetaData($record['extra']);
    }

    // Use the backtrace that Drupal passed with the log record.
    if ($backtrace) {
      $frames = &$report->getStacktrace()->getFrames();

      $this->replaceStacktraceFrames(
        $frames,
        Stacktrace::fromBacktrace($this->client->getConfig(), $backtrace, $backtrace[0]['file'], $backtrace[0]['line'])
      );
    }

    $this->client->notify($report);
  }

  private function replaceStacktraceFrames(array &$frames, Stacktrace $replacement): void {
    $frames = $replacement->getFrames();
  }

  private function writeError(array $record, string $severity): void {
    $name = sprintf('Log(%s).%s', $record['channel'], $record['level_name']);
    $message = (string) $record['message'];

    $report = Report::fromNamedError($this->client->getConfig(), $name, $message);
    $report->setSeverity($severity);
    if (isset($record['extra'])) {
      $report->setMetaData($record['extra']);
    }

    // Remove all frames related to monolog and invoking this handler.
    $stacktrace = $report->getStacktrace();
    $frames = &$stacktrace->getFrames();
    while ($frame = reset($frames)) {
      if (strpos($frame['method'], 'Drupal\\custom_logging\\') === 0) {
        $stacktrace->removeFrame(0);
      }
      elseif (strpos($frame['method'], 'Drupal\\monolog\\') === 0) {
        $stacktrace->removeFrame(0);
      }
      elseif (strpos($frame['method'], 'Monolog\\') === 0) {
        $stacktrace->removeFrame(0);
      }
      else {
        break;
      }
    }

    $this->client->notify($report);
  }

  /**
   * @param int|string $level
   */
  private function getSeverity($level): string {
    return self::SEVERITY_MAPPING[$level] ?? self::SEVERITY_MAPPING[Logger::ERROR];
  }

}
