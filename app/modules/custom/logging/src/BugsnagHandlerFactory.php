<?php

namespace Drupal\custom_logging;

use Bugsnag\Client;
use Bugsnag\Report;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\NoopHandler;

/**
 * @phpstan-import-type Level from \Monolog\Logger
 * @phpstan-import-type LevelName from \Monolog\Logger
 */
final class BugsnagHandlerFactory {

  const RELEASE_STAGES = [
    'dev' => 'development',
    'acc' => 'acceptance',
    'prod' => 'production',
  ];

  const DEFAULT_RELEASE_STAGE = 'development';

  const SAFE_POST_KEYS = [
    'form_id',
  ];

  const REDACTED_KEYS = [
    '/^pass|password|passwd|email|emailaddress$/i',
  ];

  /**
   * @phpstan-param Level|LevelName $level
   */
  public static function create($level, string $app_root): HandlerInterface {
    $api_key = getenv('BUGSNAG_API_KEY');
    if (!$api_key) {
      return new NoopHandler();
    }

    $client = Client::make($api_key);
    $client->setProjectRoot(dirname($app_root));
    $client->setReleaseStage(self::RELEASE_STAGES[getenv('APP_ENV')] ?? self::DEFAULT_RELEASE_STAGE);
    $client->setRedactedKeys(self::REDACTED_KEYS);
    $client->registerCallback(function (Report $report) {
      $message = self::sanitizeValue($report->getMessage());
      assert(is_string($message));
      $report->setMessage($message);

      $metadata = $report->getMetaData();
      $request_method = $metadata['request']['httpMethod'] ?? NULL;

      if ($request_method === 'POST') {
        array_walk_recursive($metadata['request']['params'], function (&$value, $key) {
          if (is_string($value) && strpos($value, '@') > 0 && !in_array($key, self::SAFE_POST_KEYS)) {
            $value = '[FILTERED]';
          }
        });
      }

      if (isset($metadata['user'])) {
        $metadata['user'] = self::sanitizeValue($metadata['user']);
      }

      assert(is_array($metadata));
      $report->setMetaData($metadata, FALSE);
    });

    return new BugsnagHandler($client, $level);
  }

  private static function sanitizeValue(mixed $value): mixed {
    if (is_string($value) && strpos($value, '@') > 0) {
      $value = preg_replace('/(\S)\S*(@\S+)/', '$1[FILTERED]$2', $value);
    }
    return $value;
  }

}
