<?php

namespace Drupal\custom_admin\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

final class AdminThemeNegotiator implements ThemeNegotiatorInterface {

  public function __construct(
    private readonly ConfigFactoryInterface $configFactory
  ) {
  }

  public function applies(RouteMatchInterface $route_match): bool {
    $route = $route_match->getRouteObject();
    $route_name = $route_match->getRouteName();
    return (($route && $route->getOption('_custom_admin_theme') === TRUE)
      || in_array($route_name, [
        'entity.taxonomy_term.canonical',
        'entity.node.canonical',
        'entity.user.canonical',
        'user.login',
        'user.pass',
      ])
    || str_starts_with($route_name, 'view.'));
  }

  public function determineActiveTheme(RouteMatchInterface $route_match): string {
    $theme = $this->configFactory->get('system.theme')->get('admin');
    assert(is_string($theme));
    return $theme;
  }

}
