<?php

namespace Drupal\custom_admin\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\Routing\Route;

final class SubMenuAccessCheck implements AccessInterface {

  protected CacheBackendInterface $cache;

  protected CacheContextsManager $cacheContextsManager;

  protected MenuLinkManagerInterface $menuLinkManager;

  protected MenuLinkTreeInterface $menuTree;

  public function __construct(MenuLinkManagerInterface $menu_link_manager, MenuLinkTreeInterface $menu_tree, CacheBackendInterface $cache, CacheContextsManager $cache_contexts_manager) {
    $this->cache = $cache;
    $this->cacheContextsManager = $cache_contexts_manager;
    $this->menuLinkManager = $menu_link_manager;
    $this->menuTree = $menu_tree;
  }

  public function access(AccountInterface $account, Route $route, RouteMatchInterface $route_match): AccessResultInterface {
    $cid = $this->createCacheId($route_match);
    $cache_data = $this->cache->get($cid);
    if ($cache_data) {
      assert(isset($cache_data->data));
      $access = $cache_data->data;
    }
    else {
      $access = $this->resolveAccess($account, $route, $route_match);

      // Caching is only possible if all submenu items only vary by
      // user permissions.
      if ($access->getCacheContexts() == ['user.permissions']) {
        $this->cache->set($cid, $access, $access->getCacheMaxAge(), $access->getCacheTags());
      }
    }

    return $access;
  }

  protected function createCacheId(RouteMatchInterface $route_match): string {
    $cid_parts = [
      'custom_admin_submenu_access',
      Url::fromRouteMatch($route_match)->toUriString(),
    ];
    $contexts = [
      'user.permissions',
    ];
    $contexts = $this->cacheContextsManager->convertTokensToKeys($contexts);
    $cid_parts = array_merge($cid_parts, $contexts->getKeys());

    return implode(':', $cid_parts);
  }

  protected function resolveAccess(AccountInterface $account, Route $route, RouteMatchInterface $route_match): AccessResult {
    $access = AccessResult::neutral();

    $permission = $route->getRequirement('_permission');
    if ($permission) {
      // Allow to conjunct the permissions with OR ('+') or AND (',').
      $split = explode(',', $permission);
      if (count($split) > 1) {
        $access = AccessResult::allowedIfHasPermissions($account, $split);
      }
      else {
        $split = explode('+', $permission);
        $access = AccessResult::allowedIfHasPermissions($account, $split, 'OR');
      }

      if (!$access->isAllowed()) {
        return AccessResult::forbidden()->inheritCacheability($access);
      }
    }

    $route_name = $route_match->getRouteName();
    if (!$route_name) {
      return $access;
    }

    $accessible_submenu_items = FALSE;

    $menu = $route_match->getRouteObject()?->getRequirement('_custom_admin_submenu_access');

    // Load links matching this route.
    $links = $this->menuLinkManager->loadLinksByRoute(
      $route_name,
      $route_match->getRawParameters()->all(),
      $menu
    );

    if (!$links) {
      return $access;
    }

    // Select the first matching link.
    $link = reset($links);

    $access->addCacheableDependency($link);

    $tree = $this->getChildMenuLinks($link);
    foreach ($tree as $element) {
      $access->addCacheableDependency($element->link);
      $access->inheritCacheability($element->access);

      if ($element->access->isAllowed()) {
        $accessible_submenu_items = TRUE;
      }
    }

    return AccessResult::allowedIf($accessible_submenu_items)->inheritCacheability($access);
  }

  protected function getChildMenuLinks(MenuLinkInterface $link): array {
    // Only find the children of this link.
    $link_id = $link->getPluginId();
    $parameters = new MenuTreeParameters();
    $parameters
      ->setRoot($link_id)
      ->excludeRoot()
      ->setTopLevelOnly()
      ->onlyEnabledLinks();
    $tree = $this->menuTree->load($link->getMenuName(), $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
    ];
    return $this->menuTree->transform($tree, $manipulators);
  }

}
