import Drupal from 'drupal';
import once from 'drupal-once';
import $ from 'jquery';

Drupal.behaviors.backendForm = {
  attach: function (context) {
    var $selects = $(once('backend-form-select2', 'select:not(.weight)', context));

    $selects.filter('[multiple], [required]').find('option[value="_none"]').remove();
    $selects.filter('[multiple]').select2({
      minimumResultsForSearch: 20
    });
    $selects.filter(':not([multiple])').select2({
      minimumResultsForSearch: 20,
      selectOnClose: true
    });

    $(':input', context).on('invalid', onInvalid);
  }
};

function onInvalid(e) {
  const verticalTab = $(this).parents('.vertical-tabs__pane').data('verticalTab');
  if (verticalTab) {
    verticalTab.focus();
  }

  const horizontalTab = $(this).parents('.horizontal-tabs-pane').data('horizontalTab');
  if (horizontalTab) {
    horizontalTab.focus();
  }
}
