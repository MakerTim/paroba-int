# Website [www.paroba-internal.nl](https://www.paroba-internal.nl) #

## Setup development environment ##

1. Checkout the repository.

2. Install PHP dependencies:

        composer install

3. Configure your .env file:

        bash scripts/sh/env-init.sh

4. Install NodeJS (development) dependencies:

        npm ci

      In case you use Node Version Management (nvm) run `nvm use` before installing the dependencies

5. Get a copy of the installed application (with data) from the acceptance environment:

        drush pull-acc

      Or install the application from configuration only (without data):

        drush site:install --existing-config

## Synchronizing uploads ##

By default, when running a pull command (`drush pull-acc`), we enable the
stage_file_proxy module after synchronizing the database. This will
prevent the entire uploads-public from being downloaded.

To prevent the pull commands from enabling stage_file_proxy add
`STAGE_FILE_PROXY=0` to your .env file. It will then rsync all
uploads-public in full.

By default the stage_file_proxy uses **acc** as origin. To use the uploads from
the **prod** environment add `STAGE_FILE_PROXY_ORIGIN=prod` to your .env file.

You can always disable stage_file_proxy later on by running
`drush pmu stage_file_proxy`. After a new pull the stage_file_proxy will
be re-enabled.

The configuration of the stage_file_proxy module is automatically excluded
from `drush cex`.

Note that uploads-private are always synchronized because the module doesn't
have access to those files.

## Translations ##

To extract translatables from code & templates and update the po files:

```
bash scripts/sh/update-translations.sh
```

To import translations:

```
drush locale:check
drush locale:update
drush cr
```

## Testing in development environment ##

To run acceptance tests:

```
vendor/bin/behat --profile=dev [features/...]
```

To run unit tests:
```
vendor/bin/phpunit
```
