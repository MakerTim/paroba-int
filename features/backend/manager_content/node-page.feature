Feature: Page
  In order to fill a site
  As a manager of content
  There needs to be a lane-based page content type

  Background:
    Given "page" content:
      | title      |
      | Basic page |

  @api
  Scenario: Creating a page
    Given I am logged in as a user with the "manager_content" role
    Given I am at "/node/add/page"
    Then the response status code should be 200
    When I fill in "Titel" with "Test pagina A"
    And I press the "edit-submit" button
    Then I should see the success message "Pagina Test pagina A is aangemaakt."

  @api
  Scenario: Editing a page
    Given I am logged in as a user with the "manager_content" role
    Then I should be able to edit a "page"

  @api
  Scenario: Saving a page
    Given I am logged in as a user with the "manager_content" role
    And I am on the edit form of "page" with title "Basic page"
    And I press "edit-submit"
    Then I should see the success message "Pagina Basic page is bijgewerkt."

  @api
  Scenario: Deleting a page
    Given I am logged in as a user with the "manager_content" role
    And I am on the edit form of "page" with title "Basic page"
    Then I should not see the link "edit-delete"
    Given "page" content:
      | title            | status |
      | Unpublished page | 0      |
    And I am on the edit form of "page" with title "Unpublished page"
    And I click "edit-delete"
    And I press "edit-submit"
    Then I should see the success message "Pagina Unpublished page is verwijderd."

  @api
  Scenario: Editing metatags of a page
    Given I am logged in as a user with the "manager_content" role
    Given I am at "/node/add/page"
    Then I should see the button "Metatags"
