Feature: Articles
  In order to stay up-to-date
  As a visitor
  There needs to be a article content type

  @api
  Scenario: View article item page
    Given I am viewing an "article":
      | title | My article        |
      | body  | PLACEHOLDER BODY. |
    Then I should see the heading "My article"
    And I should see the text "PLACEHOLDER BODY." in the "content" region
