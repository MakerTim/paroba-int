<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Mink\Exception\ExpectationException;
use Behat\MinkExtension\Context\RawMinkContext;

/**
 * Provides step-definitions for testing xml responses.
 */
class XmlContext extends RawMinkContext implements SnippetAcceptingContext {

  /**
   * @Then I should get a valid sitemap.xml response
   */
  public function assertValidXml() {
    $xml = new DOMDocument();
    $xml->loadXML($this->getSession()->getPage()->getContent());

    $valid = $xml->schemaValidate(__DIR__ . '/../schema/sitemap-xhtml.xsd');

    if (!$valid) {
      throw new ExpectationException('Returned xml does not match schema', $this->getSession()->getDriver());
    }
  }
}
