<?php

use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Behat\Hook\Scope\AfterScenarioScope;

/**
 * Defines application features from the specific context.
 */
class FailureHtmlDumpContext extends RawMinkContext {

  public function __construct() {
  }

  /**
   * @AfterScenario
   */
  public function takeScreenshotAfterFailedStep(AfterScenarioScope $scope) {
    if (99 === $scope->getTestResult()->getResultCode()) {
      $filename = $scope->getFeature()->getFile() . '-' . $scope->getScenario()->getLine();
      $this->takeScreenshot($filename);
    }
  }

  private function takeScreenshot($scenarioFileName) {
    $filePieces = explode('/', $scenarioFileName);

    $fileName = array_pop($filePieces) . '-' . time() . '.html';
    array_pop($filePieces);

    $html_dump_path = getcwd() . '/../storage/test-results';
    if (!file_exists($html_dump_path)) {
      mkdir($html_dump_path);
    }

    $html = $this->getSession()->getPage()->getContent();

    $htmlCapturePath = $html_dump_path . '/' . $fileName;
    file_put_contents($htmlCapturePath, $html);

    $message = "\nHTML available at: " . $htmlCapturePath;
    print $message . PHP_EOL;
  }

}
