<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Provides step-definitions for interacting with Drupal paragraphs.
 */
class ParagraphsContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paragraphStorage;

  /**
   * Keep track of paragraphs so they can be cleaned up.
   *
   * @var array
   */
  protected $paragraphs = [];

  /**
   * @var \Drupal\node\NodeInterface
   */
  protected $lastNode;

  /**
   * Initializes the context for a single scenario.
   */
  public function __construct() {
    $this->paragraphStorage = \Drupal::entityTypeManager()->getStorage('paragraph');
  }

  /**
   * Remove any created paragraphs.
   *
   * @AfterScenario
   */
  public function cleanParagraphs() {
    $this->paragraphStorage->delete($this->paragraphs);
    $this->paragraphs = [];
  }

  public function paragraphCreate(array $values) {
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $this->paragraphStorage->create($values);
    $paragraph->save();

    $this->paragraphs[$paragraph->id()] = $paragraph;

    return $paragraph;
  }

  public function paragraphDelete($paragraph) {
    $paragraph = $paragraph instanceof ParagraphInterface ? $paragraph : $this->paragraphStorage->load($paragraph->id);
    if ($paragraph instanceof ParagraphInterface) {
      $paragraph->delete();
    }
  }

  /**
   * Keeps track of the last created/visited node.
   *
   * @param \Drupal\DrupalExtension\Hook\Scope\EntityScope $scope
   *
   * @afterNodeCreate
   */
  public function afterNodeCreate(EntityScope $scope) {
    $this->lastNode = $scope->getEntity();
  }

  /**
   * Adds a paragraph of given type to given field of the last viewed content.
   *
   * The paragraph should be provided in the form:
   * | title     | My paragraph   |
   * | Field One | My field value |
   * | status    | 1              |
   * | ...       | ...            |
   *
   * @param string $field
   * @param string $type
   * @param \Behat\Gherkin\Node\TableNode $fields
   *
   * @throws \Exception
   *
   * @Given the :field contains a :type( paragraph):
   * @Given the :field contains a :type( paragraph)
   */
  public function theFieldContainsAParagraph($field, $type, TableNode $fields = null) {
    if (!$this->lastNode) {
      throw new RuntimeException('No last node available.');
    }

    $node = Node::load($this->lastNode->nid);

    if ($node instanceof NodeInterface === FALSE) {
      throw new RuntimeException('Could not load node.');
    }

    $paragraph = (object) [
      'type' => $type,
    ];
    if ($fields) {
      foreach ($fields->getRowsHash() as $field_name => $field_value) {
        $paragraph->{$field_name} = $field_value;
      }

      $this->parseEntityFields('paragraph', $paragraph);
      $this->expandEntityFields('paragraph', $paragraph);
    }
    // Cast object to array.
    $paragraph = json_decode(json_encode($paragraph), true);

    $saved = $this->paragraphCreate($paragraph);

    $node->get($field)->appendItem($saved);
    $node->save();

    // Set internal browser on the node.
    $this->getSession()->visit($this->locatePath('/node/' . $node->id()));
  }

  protected function expandEntityFields($entity_type, \stdClass $entity) {
    /** @var \Drupal\Driver\DrupalDriver $driver */
    $driver = $this->getDriver();
    $core = $driver->getCore();

    $field_types = $core->getEntityFieldTypes($entity_type);
    foreach ($field_types as $field_name => $type) {
      if (isset($entity->$field_name)) {
        $entity->$field_name = $core->getFieldHandler($entity, $entity_type, $field_name)
          ->expand($entity->$field_name);
      }
    }
  }

}
