#!/usr/bin/env bash

main() {
  if [ -x "$(command -v ddev)" ]; then
    ddev exec "$@"
  else
    "$@"
  fi
}

(
  set -euo pipefail
  main "$@"
)
