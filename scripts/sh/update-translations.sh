#!/usr/bin/env bash

check-translations-folder() {
  if ! [[ -d "app/translations" ]]
  then
    echo "Creating app/translations folder..."
    mkdir -p app/translations
  fi
}

update-translation() {
  local lang=$1
  local src=$2
  local dst=$3

  local tmpfile="app/general.pot"
  local dstfile="app/translations/${dst}.${lang}.po"

  echo "Checking $dstfile..."
  vendor/bin/drush potx single --folder="$src" --api=8 --language="$lang" --translations

    sed "
      /^\"Project-Id-Version:/ s/.*/\"Project-Id-Version: $dst\\\n\"/
      /^\"POT-Creation-Date:/ d
      /^\"PO-Revision-Date:/ d
      /^\"Last-Translator:/ d
      /^\"Language-Team:/ s/.*/\"Language: $lang\\\n\"/

      /^#: / s/:[0-9]\+\(;[0-9]\+\)*\b//g
    " "$tmpfile" > "$dstfile"
  rm "$tmpfile"
}

list-languages() {
  find app/config/default -maxdepth 1 -type f -name 'language.entity.*.yml' -exec basename "{}" \; | cut -d "." -f 3 | grep -vxE "en|und|zxx" | sort
}

main() {
  local languages; languages="$(list-languages)"

  echo "Checking translatables..."

  check-translations-folder
  vendor/bin/drush en potx -y

  for language in ${languages}; do
    update-translation "$language" "modules/custom/" "custom"
    update-translation "$language" "themes/frontend/" "frontend"
    update-translation "$language" "themes/backend/" "backend"
    update-translation "$language" "themes/mailing/" "mailing"
  done
}

finish() {
  vendor/bin/drush pmu potx -y
}

(
  set -euo pipefail
  main
  trap finish EXIT
)
