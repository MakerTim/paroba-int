#!/usr/bin/env bash

main() {
  gulp lint-changes
  bash scripts/sh/exec-on-unix.sh vendor/bin/phpcs
  bash scripts/sh/exec-on-unix.sh vendor/bin/phpmd app text phpmd.xml.dist
  bash scripts/sh/exec-on-unix.sh vendor/bin/phpstan
}

(
  set -euo pipefail
  main
)
