/* global module, require */
require('dotenv').config();

const gulp = require('gulp');
const bs = require('browser-sync').create();

const reload = function(done) {
  bs.reload();
  done();
};

module.exports = function (config) {
  return function browsersync() {
    bs.init({
      proxy: process.env.APP_URL
    });

    gulp.watch(config.path.ASSETS_SRC + '/**/*.scss', gulp.series('build-scss'));
    gulp.watch(config.path.ASSETS_SRC + '/**/*.js', gulp.series('build-js', reload));
    gulp.watch(config.path.ASSETS_SRC + '/**/*.@(gif|jpg|png|svg)', gulp.series('build-images', reload));
    gulp.watch(config.path.TWIG_TEMPLATES + '/**/*.twig', reload);
  };
};

module.exports.browsersync = bs;

module.exports.isSyncing = process.argv.includes('browsersync');

module.exports.reload = reload;
