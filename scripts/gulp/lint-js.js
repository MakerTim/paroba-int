/* global module, require */

const gulp = require('gulp');
const eslint = require('gulp-eslint');

module.exports = function (config) {
  return function lintJs() {
    return gulp.src(config.path.ASSETS_SRC + '/**/*.js')
      .pipe(eslint())
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
  };
};
