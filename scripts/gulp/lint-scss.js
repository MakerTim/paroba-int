/* global module, require */

const gulp = require('gulp');
const stylelint = require('gulp-stylelint');

module.exports = function (config) {
  return function lintScss() {
    return gulp.src(config.path.ASSETS_SRC + '/**/*.scss')
      .pipe(stylelint({
        syntax: 'scss',
        reporters: [
          {formatter: 'string', console: true}
        ]
      }))
  };
};
