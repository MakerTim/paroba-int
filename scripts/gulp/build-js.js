/* global module, require */

const rollup = require('rollup');
const terser = require('rollup-plugin-terser');
const sourcemaps = require('rollup-plugin-sourcemaps');
const buble = require('@rollup/plugin-buble');
const commonjs = require('@rollup/plugin-commonjs');
const resolve = require('@rollup/plugin-node-resolve');
const glob = require("glob");

const globals = {
  'drupal': 'Drupal',
  'drupal-settings': 'drupalSettings',
  'jquery': 'jQuery'
};

const external = [
  'drupal',
  'drupal-settings',
  'jquery'
];

const plugins = [
  resolve({
    extensions: [".js"]
  }),
  commonjs(),
  sourcemaps(),
  buble()
];

module.exports = function (config) {
  return function buildJs() {
    const entries = glob.sync('**/*.entry.js', { cwd: config.path.ASSETS_SRC });

    let promises = [];

    entries.forEach((entry) => {
      console.log('Processing', entry);

      const options = {
        external,
        plugins,
        input: config.path.ASSETS_SRC + '/' + entry
      };

      promises.push(rollup.rollup(options).then(bundle => {
        return Promise.all([
          bundle.write({
            file: config.path.ASSETS_DST + '/' + entry.replace(/\.entry\.js$/, '.js'),
            format: 'iife',
            globals
          }),
          bundle.write({
            sourcemap: false, // TODO
            file: config.path.ASSETS_DST + '/' + entry.replace(/\.entry\.js$/, '.min.js'),
            format: 'iife',
            globals,
            plugins: [terser.terser({
              mangle: {
                reserved: ['Drupal'],
              },
              output: {
                comments: false
              }
            })]
          })
        ]);
      }));
    });

    return Promise.all(promises);
  };
};
