/* global module, require */

const gulp = require('gulp');
const newer = require('gulp-newer');
const favicons = require('gulp-favicons');

module.exports = function (config) {
  return function buildFavicons() {
    return gulp.src(config.path.ASSETS_SRC + '/icon.png')
      .pipe(newer(config.path.FAVICONS_DST + '/favicon.ico'))
      .pipe(favicons({
        background: '#fff',
        version: 1.0,
        logging: false,
        online: false,
        replace: true,
        icons: {
          android: false,
          appleIcon: true,
          appleStartup: false,
          coast: false,
          favicons: true,
          firefox: false,
          opengraph: false,
          twitter: false,
          windows: true,
          yandex: false
        }
      }))
      .on('error', console.log)
      .pipe(gulp.dest(config.path.FAVICONS_DST));
  };
};
