<?php

declare(strict_types=1);

namespace Drush\Commands;

use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Consolidation\SiteProcess\SiteProcess;
use Drush\Attributes as CLI;
use Drush\Drush;
use Drush\SiteAlias\ProcessManager;
use Symfony\Component\Process\Process;

final class PullDataCommands extends DrushCommands implements SiteAliasManagerAwareInterface {
  use SiteAliasManagerAwareTrait;

  public const PULL  = 'pull';

  /**
   * Pull data from another environment.
   */
  #[CLI\Command(name: self::PULL)]
  #[CLI\Argument(name: 'source', description: 'The environment to pull data from.')]
  #[CLI\Usage(name: 'drush pull acc', description: 'Pull data from the acc environment.')]
  #[CLI\Usage(name: 'drush pull prod', description: 'Pull data from the prod environment.')]
  public function pull(string $source): void {
    $status = $this->drushProcess('status', [], ['fields' => 'bootstrap', 'format' => 'json'])
      ->mustRun()
      ->getOutputAsJson();

    if (!empty($status)) {
      $config_changes = $this->drushProcess('config:status', [], ['format' => 'json'])
        ->mustRun()
        ->getOutputAsJson();
      if (!empty($config_changes)) {
        $this->io()->writeln(sprintf('You have %s unexported configuration files:', count($config_changes)));
        $this->io()->newLine();
        $this->io()->table(['Name', 'State'], $config_changes);

        if (!$this->io()->confirm('Ignore configuration and continue?')) {
          return;
        }
      }
      else {
        $this->io()->writeln('You have no unexported configuration files.');
      }
    }

    $this->drush('sql:create');

    $this->drush('sql:sync', ["@$source", '@self']);

    $this->drush('sql:sanitize');

    if (getenv('STAGE_FILE_PROXY') !== '0') {
      $this->drush('install', ['stage_file_proxy']);
    }
    else {
      $this->drush('rsync', ["@$source:%files", '@self:%files'], ['exclude-paths' => 'css:js:styles:google_tag']);
    }

    $this->drush('rsync', ["@$source:%private", '@self:%private']);

    $this->drush('cache:rebuild');
  }

  private function drush(string $command, array $args = [], array $options = []): SiteProcess {
    $process = $this->drushProcess($command, $args, $options);
    $process->setTty($this->input()->isInteractive());
    $process->setPty($this->input()->isInteractive() && Process::isPtySupported());
    $process->mustRun($process->showRealtime());

    return $process;
  }

  private function drushProcess(string $command, array $args = [], array $options = []): SiteProcess {
    $self = $this->siteAliasManager()->getSelf();
    $manager = $this->processManager();
    assert($manager instanceof ProcessManager);

    $options = array_merge(Drush::redispatchOptions(), $options);

    $process = $manager->drush($self, $command, $args, $options);
    assert($process instanceof SiteProcess);

    return $process;
  }

}
