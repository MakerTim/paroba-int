<?php

declare(strict_types=1);

namespace Drush\Hooks;

use Consolidation\AnnotatedCommand\CommandResult;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Consolidation\SiteProcess\ProcessBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drush\Attributes as CLI;
use Drush\Commands\config\ConfigImportCommands;
use Drush\Commands\core\DeployCommands;
use Drush\Commands\core\LocaleCommands;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\CommandFailedException;
use Drush\SiteAlias\ProcessManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class DeployHooks extends DrushCommands implements SiteAliasManagerAwareInterface {
  use SiteAliasManagerAwareTrait;

  public function __construct(
    private readonly StateInterface $state,
    private readonly ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct();
  }

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('state'),
      $container->get('module_handler'),
    );
  }

  #[CLI\Hook(type: HookManager::PRE_COMMAND_HOOK, target: DeployCommands::DEPLOY)]
  public function preDeploy(): ?CommandResult {
    $this->logger()?->success('Deploy update start.');

    $app_version = getenv('APP_VERSION');
    $app_version_prev = $this->state->get('deploy.app_version');
    assert(is_string($app_version_prev));

    $this->io()->writeln(sprintf('>  [notice] Update from: %s', $app_version_prev));
    $this->io()->writeln(sprintf('>  [notice] Update to:   %s', $app_version));

    if (getenv('APP_ENV') === 'dev') {
      $this->io()->writeln('>  [notice] Update enforced; dev environment.');
      return null;
    }

    if ($app_version && $app_version === $app_version_prev) {
      $this->io()->writeln('>  [notice] Update aborted; already up-to-date.');

      return CommandResult::exitCode(0);
    }

    return null;
  }

  #[CLI\Hook(type: HookManager::POST_COMMAND_HOOK, target: DeployCommands::DEPLOY)]
  public function importTranslations(): void {
    $this->logger()?->success('Translations import start.');

    if (!$this->moduleHandler->moduleExists('locale')) {
      $this->io()->writeln('>  [notice] Translations import skipped; locale module not enabled.');
      return;
    }

    $translations_custom = glob('../app/translations/*.po');
    assert(is_array($translations_custom));
    $translations_contrib = glob('../storage/translations/*.po');
    assert(is_array($translations_contrib));

    $checksum = hash_final(array_reduce(
      [
        ...$translations_custom,
        ...$translations_contrib,
      ],
      fn ($ctx, $filename) => match(hash_update_file($ctx, $filename)) {
        true => $ctx,
        default => throw new CommandFailedException(sprintf('Failed to create checksum for %s', $filename)),
      },
      hash_init('crc32')
    ));
    $checksum_prev = $this->state->get('deploy.checksum_translations');
    assert(is_string($checksum_prev));

    $this->io()->writeln(sprintf('>  [notice] Checksum old: %s', $checksum_prev));
    $this->io()->writeln(sprintf('>  [notice] Checksum new: %s', $checksum));

    if ($checksum === $checksum_prev) {
      $this->io()->writeln('>  [notice] Translations import aborted; already up-to-date.');
      return;
    }

    $this->drush(LocaleCommands::CHECK);
    $this->drush(LocaleCommands::UPDATE);

    // Ensure custom translations have precedence over contrib translations.
    $this->drush(LocaleCommands::IMPORT_ALL, ['translations'], ['override' => 'all']);
    // Revert overrides of default config during translation import.(config that was config/install).
    // @todo check if this is still necessary.
    // @link https://www.drupal.org/project/drupal/issues/2806009
    $this->drush( ConfigImportCommands::IMPORT);

    $this->state->set('deploy.checksum_translations', $checksum);

    $this->io()->writeln('>  [notice] Translations imported.');
  }

  #[CLI\Hook(type: HookManager::POST_COMMAND_HOOK, target: DeployCommands::DEPLOY)]
  public function postDeploy(): void {
    $this->logger()?->success('Deploy update finished.');

    $app_version = getenv('APP_VERSION');
    $app_version_prev = $this->state->get('deploy.app_version');
    assert(is_string($app_version_prev));

    $this->io()->writeln(sprintf('>  [notice] Update from: %s', $app_version_prev));
    $this->io()->writeln(sprintf('>  [notice] Update to:   %s', $app_version));

    $this->state->set('deploy.app_version', $app_version);
  }

  private function drush(string $command, array $args = [], array $options = []): ProcessBase {
    $manager = $this->processManager();
    assert($manager instanceof ProcessManager);
    $self = $this->siteAliasManager()->getSelf();

    $process = $manager->drush($self, $command, $args, $options);
    $process->mustRun($process->showRealtime());

    return $process;
  }

}
